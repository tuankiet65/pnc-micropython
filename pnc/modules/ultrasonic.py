import pyb

from ..port import get_port

class Ultrasonic:
	port = None
	trigger = None
	echo = None

	timer = None

	def __init__(self, port):
		self.port = get_port(port)
		self.trigger = self.port.pin1
		self.echo = self.port.pin2

		self.trigger.init(mode = pyb.Pin.OUT)
		self.echo.init(mode = pyb.Pin.IN)

	def measure(self) -> float:
		# Creating a timer ticking every 1us
		self.timer = pyb.Timer(2, prescaler = (pyb.freq()[2] * 2 // (10**6)) - 1, period = 0x3fffffff)

		self.trigger.value(True)
		pyb.udelay(20)
		self.trigger.value(False)
		
		while self.echo.value() == False:
			pass

		self.timer.counter(0)

		while self.echo.value() == True:
			pass

		duration = self.timer.counter()
		self.timer.deinit()

		if (duration > 50000):
			return float("inf")

		return (duration * 1e-6) * 330.0 * 100.0 / 2.0
