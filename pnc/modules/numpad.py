import pyb

from .base import CustomUARTModuleBase
from ..uart_request.value import *


class Numpad_4x4(CustomUARTModuleBase):
    column = None
    row = None

    CMD_GET_BUTTON = const(0x0a)

    def get_module_info(self):
        req = self.send(self.CMD_INFO, ret = [
            ("type", UInt8Value()),
            ("column", UInt8Value()),
            ("row", UInt8Value())
        ])

        self.column = req.data["column"]
        self.row = req.data["row"]

    def init(self):
        pass

    @staticmethod
    def _parse_row(row_bin: int, row_char: str):
        result = ""

        for i in range(4):
            if (row_bin & (1 << i)) != 0:
                result += row_char[i]

        return result

    def get_button(self) -> str:
        req = self.send(self.CMD_GET_BUTTON, ret = [
            ("row1", UInt8Value()),
            ("row2", UInt8Value()),
            ("row3", UInt8Value()),
            ("row4", UInt8Value())
        ])

        return (self._parse_row(req.data['row1'], '123A') +
                self._parse_row(req.data['row2'], '456B') +
                self._parse_row(req.data['row3'], '789C') +
                self._parse_row(req.data['row4'], '*0#D'))

    def wait_for_any(self, buttons: str = "1234567890*#ABCD", timeout: int = None):
        start_time = pyb.millis()

        while True:
            for pressed_button in self.get_button():
                if pressed_button in buttons:
                    return pressed_button

            if timeout is not None:
                if pyb.elapsed_millis(start_time) > timeout:
                    return ''
