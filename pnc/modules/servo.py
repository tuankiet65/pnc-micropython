import pyb

from ..port import get_port


class Servo:
    timer1 = None
    timer2 = None

    pin1 = None
    pin2 = None

    channel1 = None
    channel2 = None

    def __init__(self, port):
        port = get_port(port)

        if not port.pwm_info['available']:
            raise ValueError("This port does not support PWM")

        self.pin1 = port.pin1
        self.pin2 = port.pin2

        self.timer1 = pyb.Timer(port.pwm_info['pin1']['timer'], freq = 50)
        self.timer2 = pyb.Timer(port.pwm_info['pin2']['timer'], freq = 50)

        self.channel1 = port.pwm_info['pin1']['channel']
        self.channel2 = port.pwm_info['pin2']['channel']

    def angle(self, servo_num, angle):
        if (angle > 180):
            raise ValueError("angle must be between 0 and 180")

        # percent can go from 2.5% to 12.5%
        percent = (angle / 18) + 2.5

        if servo_num == 1:
            self.timer1.channel(self.channel1,
                                mode = pyb.Timer.PWM,
                                pulse_width_percent = percent,
                                pin = self.pin1)
        else:
            self.timer2.channel(self.channel2,
                                mode = pyb.Timer.PWM,
                                pulse_width_percent = percent,
                                pin = self.pin2)
