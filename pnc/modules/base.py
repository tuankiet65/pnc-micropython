from ..port import get_port
from ..uart_request.request import Request, PacketResponse


class PortType:
    ANALOG = const(0x00)
    DIGITAL = const(0x02)
    UART = const(0x03)
    I2C = const(0x04)


class CustomUARTModuleBase:
    port = None

    CMD_INFO = const(0x00)

    def get_module_info(self):
        raise NotImplementedError

    def __init__(self, port_id):
        port = get_port(port_id)

        self.port = Request(port)
        self.get_module_info()

    def send(self, *args, **kwargs) -> PacketResponse:
        result = self.port.send(*args, **kwargs)
        return result
