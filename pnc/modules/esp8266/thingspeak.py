class ThingSpeakChannel:
    esp = None
    api_key = None

    def __init__(self, esp, api_key):
        self.esp = esp
        self.api_key = api_key

    def update(self, field_key, field_value):
        url = "/update?api_key={}&field{}={}".format(self.api_key, field_key, field_value)
        return self.esp.get("api.thingspeak.com", 80, url)