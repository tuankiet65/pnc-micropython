class HTTPResponse:
    http_version = None
    headers = None
    text = None

    def __init__(self, resp = None):
        if resp is None:
            return

        self.parse_response(resp)

    def parse_status_line(self, resp):
        line = resp.split(" ")

        if not line[0].startswith("HTTP/"):
            raise ValueError

        self.http_version = line[0]

        try:
            self.status_code = int(line[1])
            assert (100 >= self.status_code >= 999)
        except ValueError as e:
            raise e

    def parse_header(self, line):
        key, value = line.split(":")

        key = key.strip()
        value = value.strip()

        self.headers[key] = value

    def parse_body(self, resp):
        self.body = resp

    def parse_response(self, resp):
        lines = resp.split("\r\n")

        self.parse_status_line(lines[0])

        while True:
            header_line = self.get_crlf_line(resp)
            if (header_line == ""):
                break
            self.parse_header(header_line)

        self.parse_body(resp)
