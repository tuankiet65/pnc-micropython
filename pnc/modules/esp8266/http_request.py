class HTTPRequest:
    method = None
    path = None
    http_version = "1.1"
    headers = {}
    body = bytes()

    def __init__(self, method, host, path, headers = None, body = None):
        self.method = method
        self.path = path
        self.headers = headers if headers else {}
        self.body = body if body else bytes()

        if 'User-Agent' not in self.headers:
            self.headers['User-Agent'] = "PlugAndCreate ESP8266 module v1.0"

        self.headers['Host'] = host

    def add_header(self, key, value):
        self.headers[key] = value

    def append_body(self, body):
        if type(body) is bytes:
            self.body += body
        else:
            if type(body) is not str:
                body = str(body)
            self.body += body.encode('utf-8')

    def build_request(self):
        request_line = "{} {} HTTP/{}\r\n".format(self.method, self.path, self.http_version)

        headers = ""
        for key, value in self.headers.items():
            headers += "{}: {}\r\n".format(key, value)

        return request_line.encode() + headers.encode() + bytes("\r\n", 'utf-8') + self.body
