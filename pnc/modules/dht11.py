import pyb


class DHT11:
    TOTAL_FALLING_EDGES = const(43)

    pin = None
    micros = None
    falling_timing = None
    index = None

    def __init__(self, pin):
        self.pin = pin

    def _edge_falling_callback(self, _):
        self.falling_timing[self.index] = self.micros.counter()
        self.micros.counter(0)
        self.index = (self.index + 1) % self.TOTAL_FALLING_EDGES  # in case overflow occurs

    def _send_request(self):
        self.pin.init(pyb.Pin.OUT_PP)
        self.pin.value(False)
        pyb.delay(20)

    def _decode_data(self):
        result = [0, 0, 0, 0, 0]
        for i in range(40):
            bit = 1 if (self.falling_timing[i] > 90) else 0
            result[i // 8] = (result[i // 8] << 1) + bit
        print(result)
        humidity = result[0] + (result[1] / 100)
        temperature = result[2] + (result[3] / 100)
        parity = sum(result[0:3]) & 0xff
        if parity != result[-1]:
            raise ValueError("DHT11 checksum failed")
        return temperature, humidity

    def _read_init(self):
        self.index = 0
        self.falling_timing = [0] * self.TOTAL_FALLING_EDGES

        # Creating a timer ticking every 1us
        self.micros = pyb.Timer(2, prescaler = (pyb.freq()[2] * 2 // (10**6)) - 1, period = 0x3fffffff)

    def read(self):
        self._read_init()
        _ = pyb.ExtInt(self.pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self._edge_falling_callback)
        # step 1: sending request
        self._send_request()
        # step 2: releasing pin, start reading in
        self.micros.counter(0)
        self.pin.init(pyb.Pin.IN)
        pyb.delay(5)
        _ = pyb.ExtInt(self.pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, None)
        # ignore first two edges
        self.falling_timing = self.falling_timing[3:]
        return self._decode_data()