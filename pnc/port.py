import pyb


class UARTPort(pyb.UART):
    pass


class DigitalPort:
    pin1 = None
    pin2 = None
    pwm_info = None

    def __init__(self, pin1: str, pin2: str, pwm_info: dict):
        self.pin1 = pyb.Pin(pin1)
        self.pin2 = pyb.Pin(pin2)
        self.pwm_info = pwm_info


class AnalogPort:
    pin1 = None
    pin2 = None

    def __init__(self, pin1: str, pin2: str):
        self.pin1 = pyb.ADC(pin1)
        self.pin2 = pyb.ADC(pin2)


class I2CPort(pyb.I2C):
    def __init__(self, port):
        super().__init__(port)
        self.init(pyb.I2C.MASTER)


PORTS = {
    "UART1": UARTPort(1),
    "UART2": UARTPort(2),
    "UART3": UARTPort(6),

    # "A1"   : AnalogPort("PC4", "PC5"),
    # "A2"   : AnalogPort("PA6", "PA7"),
    # "A3"   : AnalogPort("PA4", "PA5"),
    # "A4"   : AnalogPort("PC2", "PC3"),

    "DIGITAL1"   : DigitalPort('PB8', 'PB7', {
        "available": True,
        "pin1"     : {
            "timer"  : 4,
            "channel": 3
        },
        "pin2"     : {
            "timer"  : 4,
            "channel": 2
        }
    }),

    # "DIGITAL2"   : DigitalPort('PB3', 'PA15', {
    #     "available": True,
    #     "pin1"     : {
    #         "timer"  : 2,
    #         "channel": 2
    #     },
    #     "pin2"     : {
    #         "timer"  : 2,
    #         "channel": 1
    #     }
    # }),

    "DIGITAL3"   : DigitalPort('PE14', 'PE13', {
        "available": True,
        "pin1"     : {
            "timer"  : 1,
            "channel": 3
        },
        "pin2"     : {
            "timer"  : 1,
            "channel": 4
        }
    }),

    "DIGITAL4"   : DigitalPort('PB0', 'PB1', {
        "available": True,
        "pin1"     : {
            "timer"  : 3,
            "channel": 3
        },
        "pin2"     : {
            "timer"  : 3,
            "channel": 4
        }
    }),

    "I2C1" : I2CPort(1),
    "I2C2" : I2CPort(1),
    "I2C3" : I2CPort(1),
    "I2C4" : I2CPort(1),
}

# PORTS = {
#     "UART1": UARTPort(1),
#     "UART2": UARTPort(2),
#     "UART3": UARTPort(6),

#     "DIGITAL1"   : DigitalPort('PD12', 'PD13', {
#         "available": True,
#         "pin1"     : {
#             "timer"  : 4,
#             "channel": 1
#         },
#         "pin2"     : {
#             "timer"  : 4,
#             "channel": 2
#         }
#     }),

#     "DIGITAL2"   : DigitalPort('PD14', 'PD15', {
#         "available": True,
#         "pin1"     : {
#             "timer"  : 4,
#             "channel": 3
#         },
#         "pin2"     : {
#             "timer"  : 4,
#             "channel": 4
#         }
#     })
# }

def get_port(port: str):
    try:
        return PORTS[port]
    except KeyError:
        raise UnknownPort(port)


class UnknownPort(Exception):
    pass
